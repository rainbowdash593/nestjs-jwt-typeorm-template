import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { ConfigService } from '@nestjs/config';

async function bootstrap() {
  const basePath = '/api/v1';
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);
  const config = new DocumentBuilder()
    .setTitle('API Methods')
    .setDescription('API Methods List')
    .setVersion('1.0')
    .addTag('api')
    .addBearerAuth()
    .addServer(basePath)
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('swagger', app, document);
  app.enableCors();
  app.setGlobalPrefix(basePath);
  await app.listen(configService.get('port', 3000));
}
bootstrap();
