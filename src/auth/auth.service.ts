import * as bcrypt from 'bcrypt';
import { BadRequestException, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../users/users.service';
import { SignInDto } from './dto/sign-in.dto';
import { User } from 'src/users/entities/user.entity';
import { IReadableUser } from '../users/interfaces/readable-user.interface';
import { CreateUserDto } from '../users/dto/create-user.dto';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async generateJwtToken(user: User): Promise<string> {
    const payload = {
      id: user.id,
      uuid: user.uuid,
    };
    return this.jwtService.sign(payload);
  }

  async validateUserByID(userId: number): Promise<IReadableUser> {
    const user = await this.usersService.findOne(userId);
    return this.usersService.toReadableUser(user);
  }

  async login({ email, password }: SignInDto): Promise<IReadableUser> {
    const user = await this.usersService.findByEmail(email);
    if (user && (await bcrypt.compare(password, user.password))) {
      const token = await this.generateJwtToken(user);
      return this.usersService.toReadableUser(user, token);
    }
    throw new BadRequestException('Invalid credentials');
  }

  async signUp(createUserDto: CreateUserDto): Promise<IReadableUser> {
    const user = await this.usersService.create(createUserDto);
    return this.usersService.toReadableUser(user);
  }
}
