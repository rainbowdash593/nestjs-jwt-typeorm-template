import { Controller, Post, ValidationPipe, Body } from '@nestjs/common';
import { SignInDto } from './dto/sign-in.dto';
import { Public } from './guards/public-route.guard';
import { AuthService } from './auth.service';
import { ApiTags } from '@nestjs/swagger';
import { IReadableUser } from '../users/interfaces/readable-user.interface';
import { CreateUserDto } from '../users/dto/create-user.dto';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Public()
  @Post('sign-in')
  async login(
    @Body(new ValidationPipe()) signInDto: SignInDto,
  ): Promise<IReadableUser> {
    return this.authService.login(signInDto);
  }

  @Public()
  @Post('sign-up')
  async signUp(
    @Body(new ValidationPipe()) createUserDto: CreateUserDto,
  ): Promise<IReadableUser> {
    return this.authService.signUp(createUserDto);
  }
}
