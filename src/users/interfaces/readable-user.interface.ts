export interface IReadableUser {
  readonly id: number;
  readonly email: string;
  readonly isActive: boolean;
  readonly createdAt: number;
  readonly updatedAt: number;
  accessToken?: string;
}
