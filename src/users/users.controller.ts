import { Controller, Get, Post, Request } from '@nestjs/common';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { IReadableUser } from './interfaces/readable-user.interface';

@ApiBearerAuth()
@ApiTags('users')
@Controller('users')
export class UsersController {
  @Post('/info')
  async info(@Request() req): Promise<IReadableUser> {
    return req.user;
  }
}
