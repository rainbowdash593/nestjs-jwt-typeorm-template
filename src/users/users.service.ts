import * as _ from 'lodash';
import * as moment from 'moment';
import * as bcrypt from 'bcrypt';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { classToPlain } from 'class-transformer';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';
import { CreateUserDto } from './dto/create-user.dto';
import { IReadableUser } from './interfaces/readable-user.interface';
import { ProtectedUserFields } from './enums/protected-fields.enum';

@Injectable()
export class UsersService {
  private readonly saltRounds = 10;
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  async hashPassword(password: string): Promise<string> {
    const salt = await bcrypt.genSalt(this.saltRounds);
    return await bcrypt.hash(password, salt);
  }

  async findAll(): Promise<User[]> {
    return this.usersRepository.find();
  }

  async findOne(id: number): Promise<User> {
    return this.usersRepository.findOne(id);
  }

  async findByEmail(email: string): Promise<User> {
    return this.usersRepository.findOne({ where: { email } });
  }

  toReadableUser(user: User, token = null): IReadableUser {
    const readableUser = classToPlain(user) as IReadableUser;
    if (token) readableUser.accessToken = token;
    return _.omit(
      readableUser,
      Object.values(ProtectedUserFields),
    ) as IReadableUser;
  }

  async create(createUserDto: CreateUserDto): Promise<User> {
    const hash = await this.hashPassword(createUserDto.password);
    return this.usersRepository.save(
      _.assignIn(createUserDto, {
        password: hash,
        isActive: true,
        createdAt: moment().toDate(),
        updatedAt: moment().toDate(),
      }),
    );
  }

  async remove(id: string): Promise<void> {
    await this.usersRepository.delete(id);
  }
}
